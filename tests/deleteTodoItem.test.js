import chai from 'chai';
import { api } from '../framework/controllers/index.js';
import { helper } from '../framework/helpers/todo.helper.js';

const { expect } = chai;

const FAKE_ID = 'qwerty';

describe('Delete to-do item tests', () => {
    it('Delete existing item', async () => {
        let id = await helper.getFirstItemId();
        let count = await helper.getItemsCount();

        let response = await api.DeleteTodoitem(id).deleteItem();
        expect(response.status).to.equal(200);
        expect(await helper.getItemsCount(), 'Items count should be decremented').to.equal(count - 1);
    })
    it('Delete non-existing item', async () => {
        let response = await api.DeleteTodoitem(FAKE_ID).deleteItem();
        expect(response.status).to.equal(404);
    })
    it('No id specified', async () => {
        let response = await api.DeleteTodoitem().deleteItem();
        expect(response.status).to.equal(404);
    })
    it('Unauthorized request', async () => {
        let response = await api.DeleteTodoitem(FAKE_ID).deleteItem(false);
        expect(response.status).to.equal(401);
    })
});