import chai from 'chai';
import { helper } from '../framework/helpers/todo.helper.js';
import { GetTodoList } from '../framework/controllers/GetTodoList.controller.js';
import { api } from '../framework/controllers/index.js';

const { expect } = chai;

const TEXT = 'asdfgh';
const DUE_DATE = '2022-11-11';
const FAKE_ID = 'qwerty';

describe('Update to-do item tests', async () => {
    it('Update item: text', async () => {
        let timestamp = Date.now();
        let id = await helper.getFirstItemId();

        let response = await api.UpdateTodoItem(id).update(TEXT);
        expect(response.status).to.equal(200);

        let item = await helper.getItemObj(id);
        expect(item.content.text, `Text should be changed to ${TEXT}, but actual is ${item.content.text}`)
            .to.equal(TEXT);
        expect(item.updated.timestamp, 'Updated time should be refreshed').to.greaterThan(timestamp);
    })
    it('Update item: dueDate', async () => {
        let id = await helper.getFirstItemId();

        let response = await api.UpdateTodoItem(id).update(undefined, DUE_DATE);
        expect(response.status).to.equal(200);

        let item = await helper.getItemObj(id);
        expect(item.dueDate.iso, `Due date should be changed to ${DUE_DATE}, but actual is ${item.dueDate.iso}`)
            .to.equal(DUE_DATE);
    })
    it('Update item: is open', async () => {
        let id = await helper.getFirstItemId();
        let item = await helper.getItemObj(id);
        let status = item._status;
        console.log(status);
        let isOpen = await helper.statusToBool(item._status);

        let response = await api.UpdateTodoItem(id).update(undefined, undefined, !isOpen);
        expect(response.status).to.equal(200);

        item = await helper.getItemObj(id);
        expect(item._status, `Status should be inversed from ${status}, but actual is ${item._status}`)
            .not.to.equal(status);
    })
    it('Update item: no changes', async () => {
        let id = await helper.getFirstItemId();
        let item = JSON.stringify(await helper.getItemObj(id));

        let response = await api.UpdateTodoItem(id).update();
        expect(response.status).to.equal(200);
        expect(JSON.stringify(await helper.getItemObj(id)), 'Item should not be changed').to.equal(item);
    })
});
describe('Update to-do item tests: negative tests', async () => {
    it('Empty text', async () => {
        let id = await helper.getFirstItemId();
        let response = await api.UpdateTodoItem(id).update('');
        expect(response.status, 'Should not be able to update with empty text').to.equal(400);
    })
    it('Wrong id', async () => {
        let response = await api.UpdateTodoItem(FAKE_ID).update();
        expect(response.status, 'Should not be able to update with not existing id').to.equal(404);
    })
    it('Without authorization', async () => {
        let id = await helper.getFirstItemId();
        let response = await api.UpdateTodoItem(FAKE_ID).update(undefined, undefined, undefined, false);
        expect(response.status, 'Should not be able to update without authorization').to.equal(401);
    })
});