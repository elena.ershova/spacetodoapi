import chai from 'chai';
import { api } from '../framework/controllers/index.js';
import { user } from '../framework/conf/user.js'
import { helper } from '../framework/helpers/todo.helper.js';

const { expect } = chai;

describe('Get all to-do lists tests with no additional params.', () => {
    it('No params request with authorization', async () => {
        let response = await api.GetTodoList().getList();
        expect(response.status, 'Request code should be 200').to.equal(200);

        let responseObj = await response.json();
        expect(responseObj.totalCount, `Total count for user ${user.username} should be > 0`)
            .to.greaterThan(0);
        expect(responseObj.data.length, 'Data items count should be equal total count')
            .to.equal(responseObj.totalCount);

        let dataItem = responseObj.data[0];
        expect(dataItem.id, 'No data id').not.to.be.null;
        expect(dataItem.created, 'No created time').not.to.be.null;
        expect(dataItem.updated, 'No updated time').not.to.be.null;
        expect(dataItem.content, 'No content').not.to.be.null;
        expect(dataItem._status, 'No status').not.to.be.null;
        expect(dataItem.dueDate, 'No due date').not.to.be.null;
    })
    it('Not authorized user', async () => {
        let r = await api.GetTodoList().getListNotAuthorized();
        expect(r.status, 'Request code should be unauthorized: 401').to.equal(401);
    })
});

describe('Get all to-do items with skip param', async () => {
    it('Numerical value in range', async () => {
        let skip = 1;
        let total = await helper.getItemsCount();

        let response = await api.GetTodoList().getList(skip);
        expect(response.status, 'Request code should be 200').to.equal(200);
        let responseObj = await response.json();
        expect(responseObj.totalCount, `Total count for user ${user.login} should be ${total}`)
            .to.equal(total);
        expect(responseObj.data.length, 'Data items count should be less on skip value')
            .to.equal(responseObj.totalCount - skip);
    })
    it('Numerical value out of range', async () => {
        let skip = 500;
        let total = await helper.getItemsCount();

        let response = await api.GetTodoList().getList(skip);
        expect(response.status, 'Request code should be 200').to.equal(200);
        let responseObj = await response.json();
        expect(responseObj.totalCount, `Total count for user ${user.login} should be ${total}`)
            .to.equal(total);
        expect(responseObj.data.length, 'Data items count should be less on skip value')
            .to.equal(0);
    })
    it('Wrong value', async () => {
        let skip = 'qwerty';
        let response = await api.GetTodoList().getList(skip);
        expect(response.status, 'Request code should be 400').to.equal(400);
    })
});

describe('Get all to-do items with top param', async () => {
    it('Value in range', async () => {
        let top = 1;
        let total = await helper.getItemsCount();

        let response = await api.GetTodoList().getList(undefined, top);
        expect(response.status, 'Request code should be 200').to.equal(200);
        let responseObj = await response.json();
        expect(responseObj.totalCount, `Total count for user ${user.login} should be ${total}`)
            .to.equal(total);
        expect(responseObj.data.length, 'Data items count should be as top value')
            .to.equal(top);
    })
    it('Value out of range (> total)', async () => {
        let top = 500;
        let total = await helper.getItemsCount();

        let response = await api.GetTodoList().getList(undefined, top);
        expect(response.status, 'Request code should be 200').to.equal(200);
        let responseObj = await response.json();
        expect(responseObj.totalCount, `Total count for user ${user.login} should be ${total}`)
            .to.equal(total);
        expect(responseObj.data.length, 'Data items count should be equal max (total)')
            .to.equal(total);
    })
    it('Value out of range (0)', async () => {
        let top = 0;
        let total = await helper.getItemsCount();

        let response = await api.GetTodoList().getList(undefined, top);
        expect(response.status, 'Request code should be 200').to.equal(200);
        let responseObj = await response.json();
        expect(responseObj.totalCount, `Total count for user ${user.login} should be ${total}`)
            .to.equal(total);
        expect(responseObj.data.length, 'All data items should be filtered')
            .to.equal(top);
    })
});

describe('Get all to-do items with open param', async () => {
    it('Open = true', async () => {
        let open = true;
        let openCount = await helper.getOpenCount();

        let response = await api.GetTodoList().getList(undefined, undefined, open);
        expect(response.status, 'Request code should be 200').to.equal(200);
        let responseObj = await response.json();
        expect(responseObj.totalCount, `Total open count for user ${user.login} should be ${openCount}`)
            .to.equal(openCount);
    })
    it('Open = false', async () => {
        let open = false;
        let total = await helper.getItemsCount();
        let openCount = await helper.getOpenCount();

        let response = await api.GetTodoList().getList(undefined, undefined, open);
        expect(response.status, 'Request code should be 200').to.equal(200);
        let responseObj = await response.json();
        expect(responseObj.totalCount, `Total count with filtered open for user ${user.login} should be ${total - openCount}`)
            .to.equal(total - openCount);
        expect(responseObj.data.length, `Data items count should be ${total - openCount}`)
            .to.equal(total - openCount);
    })
});

describe('Get all to-do items with from/till params', async () => {
    it('Valid from param', async () => {
        let from = '2022-01-10';
        let response = await api.GetTodoList().getList(undefined, undefined, undefined, from);
        expect(response.status, 'Request code should be 200').to.equal(200);
        let responseObj = await response.json();
        expect(responseObj.totalCount, `Total count for user ${user.login} should be > 0`)
            .to.greaterThan(0);
    })
    it('Valid till param', async () => {
        let till = '2022-01-15';
        let response = await api.GetTodoList().getList(undefined, undefined, undefined, undefined, till);
        expect(response.status, 'Request code should be 200').to.equal(200);
        let responseObj = await response.json();
        expect(responseObj.totalCount, `Total count for user ${user.login} should be > 0`)
            .to.greaterThan(0);
    })
    it('Valid from & till params', async () => {
        let from = '2022-01-10';
        let till = '2022-01-15';
        let response = await api.GetTodoList().getList(undefined, undefined, undefined, from, till);
        expect(response.status, 'Request code should be 200').to.equal(200);
        let responseObj = await response.json();
        expect(responseObj.totalCount, `Total count for user ${user.login} should be > 0`)
            .to.greaterThan(0);
    })
    it('From param in future', async () => {
        let from = '2025-01-10';
        let response = await api.GetTodoList().getList(undefined, undefined, undefined, from);
        expect(response.status, 'Request code should be 200').to.equal(200);
        let responseObj = await response.json();
        expect(responseObj.totalCount, `Total count for user ${user.login} should be 0`)
            .to.equal(0);
    })
    it('Till param in past', async () => {
        let from = '2000-01-10';
        let response = await api.GetTodoList().getList(undefined, undefined, undefined, from);
        expect(response.status, 'Request code should be 200').to.equal(200);
        let responseObj = await response.json();
        expect(responseObj.totalCount, `Total count for user ${user.login} should be 0`)
            .to.equal(0);
    })
    it('Wrong from param', async () => {
        let from = 'qwerty';
        let response = await api.GetTodoList().getList(undefined, undefined, undefined, from);
        expect(response.status, 'Request code should be 400').to.equal(400);
    })
    it('Wrong till param', async () => {
        let till = 'qwerty';
        let response = await api.GetTodoList().getList(undefined, undefined, undefined, till);
        expect(response.status, 'Request code should be 400').to.equal(400);
    })
});