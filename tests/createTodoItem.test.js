import chai from 'chai';
import { api } from '../framework/controllers/index.js';

const { expect } = chai;

//test data
const TEXT = 'qwerty';
const DUE_DATE = '2022-10-10';
const DUE_DATE_PAST = '2000-10-10'

let today = new Date();

describe('Create to-do item tests', async () => {
    it('Basic params test', async () => {
        let randomText = `${TEXT}${Math.random(5)}`;
        let today = new Date();

        let response = await api.CreateTodoItem(randomText).createItem();
        expect(response.status, 'Status should be 200').to.equal(200);

        let responseObj = await response.json();
        expect(responseObj.id, 'Id should be defined')
            .not.to.be.null;
        expect(responseObj.created.timestamp, 'Creation timestamp should be a bit greater than in test start :)')
            .to.be.greaterThan(today.valueOf());
        expect(responseObj.updated.timestamp, 'Update timestamp should be the same with creation timestamp :)')
            .to.equal(responseObj.created.timestamp);
        expect(responseObj.content.text, `Description should be ${randomText}}`)
            .to.equal(randomText);
        expect(responseObj._status, 'Status should be Open').to.equal('Open');
        expect(responseObj.dueDate.year, `Year should be ${today.getFullYear()}`).to.equal(today.getFullYear());
        expect(responseObj.dueDate.month, `Month should be ${today.getMonth() + 1}`).to.equal(today.getMonth() + 1);
        expect(responseObj.dueDate.day, `Day should be ${today.getDate()}`).to.equal(today.getDate());
    })
    it('Basic with due date', async () => {
        let randomText = `${TEXT}${Math.random(5)}`;
        let today = new Date();

        let response = await api.CreateTodoItem(randomText, DUE_DATE).createItem();
        expect(response.status, 'Status should be 200').to.equal(200);

        let responseObj = await response.json();
        expect(responseObj.id, 'Id should be defined')
            .not.to.be.null;
        expect(responseObj.created.timestamp, 'Creation timestamp should be a bit greater than in test start :)')
            .to.be.greaterThan(today.valueOf());
        expect(responseObj.updated.timestamp, 'Update timestamp should be the same with creation timestamp :)')
            .to.equal(responseObj.created.timestamp);
        expect(responseObj.content.text, `Description should be ${randomText}}`)
            .to.equal(randomText);
        expect(responseObj._status, 'Status should be Open').to.equal('Open');
        expect(responseObj.dueDate.iso, `Due date should be ${DUE_DATE}`).to.equal(DUE_DATE);
    })
});

describe('Create to-do item negative tests', async () => {
    it('No text', async () => {
        let response = await api.CreateTodoItem().createItem();
        expect(response.status, 'Status should be 400').to.equal(400);
    })
    it('Due date in past', async () => {
        let randomText = `${TEXT}${Math.random(5)}`;
        let response = await api.CreateTodoItem(randomText, DUE_DATE_PAST).createItem();
        expect(response.status, `Status should be 400, unable to create due to past: ${DUE_DATE_PAST}`).to.equal(400);
    })
    it('Unauthorized request', async () => {
        let response = await api.CreateTodoItem(TEXT).createItem(false);
        expect(response.status, 'Status should be 401').to.equal(401);
    })
});