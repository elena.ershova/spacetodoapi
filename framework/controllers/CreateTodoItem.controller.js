import { BaseTodo } from './BaseTodo.controller.js';

class CreateTodoItem extends BaseTodo {
    constructor(text, dueDate) {
        super();
        this.text = text;
        this.dueDate = dueDate;
    }

    async createItem(authorization) {
        let params = this.getParams(authorization);
        return await this.sendRequest(this.url, params);
    }

    getParams(authorization) {
        let params;

        params = {
            method: 'POST',
            body: this.getBody(),
            headers: this.headers(authorization)
        }
        return params;
    }

    getBody() {
        let body;
        if (this.dueDate === undefined) {
            body = {
                text: this.text,
            }
        }
        body = {
            text: this.text,
            dueDate: this.dueDate,
        }
        return JSON.stringify(body);
    }
}

export { CreateTodoItem }; 