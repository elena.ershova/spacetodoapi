import { CreateTodoItem } from './CreateTodoItem.controller.js';
import { GetTodoList } from './GetTodoList.controller.js';
import { UpdateTodoItem } from './UpdateTodoItem.controller.js';
import { DeleteTodoItem } from './DeleteTodoItem.controller.js';

const api = {
    GetTodoList: () => new GetTodoList(),
    CreateTodoItem: (text, dueDate) => new CreateTodoItem(text, dueDate),
    UpdateTodoItem: (id) => new UpdateTodoItem(id),
    DeleteTodoitem: (id) => new DeleteTodoItem(id),
}

export { api };
