import fetch from "node-fetch";
import { baseUrl, endpoints } from "../conf/urls.js";
import { user } from '../conf/user.js'

class BaseTodo {
    constructor() {
        this.url = `${baseUrl}${endpoints.todoList}`
    }

    async sendRequest(url, params) {
        let response = await fetch(url, params);
        return response;
    }

    headers(authorization = true) {
        if (authorization) {
            return {
                'Authorization': `Bearer ${user.token}`,
                'Accept': 'application/json'
            }
        }
        return {};
    }
}

export { BaseTodo };