import { BaseTodo } from './BaseTodo.controller.js';

class DeleteTodoItem extends BaseTodo {
    constructor(id) {
        super();
        this.id = id;
    }

    async deleteItem(authorization) {
        let params = {
            method: 'DELETE',
            headers: this.headers(authorization),
        }
        return await this.sendRequest(this.url + '/' + this.id, params);
    }
}

export { DeleteTodoItem };