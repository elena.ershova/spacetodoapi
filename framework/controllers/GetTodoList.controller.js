import { BaseTodo } from './BaseTodo.controller.js';

class GetTodoList extends BaseTodo {
    constructor() {
        super();
    }

    // method optional params
    skipKey = '$skip';
    topKey = '$top';
    openKey = 'open';
    fromKey = 'from';
    tillKey = 'till';

    async getList(skip, top, open, from, till) {
        let params = {
            method: 'GET',
            headers: this.headers(true)
        }

        let url = this.url;
        let tail = this.getParamsTail(skip, top, open, from, till);
        if (tail !== '?') {
            url = `${url}${tail}`;
        }
        return await this.sendRequest(url, params);
    }

    async getListNotAuthorized() {
        return await this.sendRequest(this.url);
    }

    getParamsTail(skipValue, topValue, openValue, fromValue, tillValue) {
        let tail = '?';
        if (skipValue !== undefined) {
            tail = `${tail}${this.skipKey}=${skipValue}`
        } else if (topValue !== undefined) {
            tail = `${tail}${this.topKey}=${topValue}`
        } else if (openValue !== undefined) {
            tail = `${tail}${this.openKey}=${openValue}`
        } else if (fromValue !== undefined) {
            tail = `${tail}${this.fromKey}=${fromValue}`
        } else if (tillValue !== undefined) {
            tail = `${tail}${this.tillKey}=${tillValue}`
        }
        return tail;
    }
}

export { GetTodoList };