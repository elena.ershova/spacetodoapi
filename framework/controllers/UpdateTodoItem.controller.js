import { BaseTodo } from './BaseTodo.controller.js';

class UpdateTodoItem extends BaseTodo {
    constructor(id) {
        super();
        this.id = id;
    }

    async update(text, dueDate, open, authorization) {
        let body = this.getBody(text, dueDate, open);
        return await this.sendRequest(this.url + `/${this.id}`, this.getParams(body, authorization))
    }

    getParams(body, authorization) {
        return {
            method: 'PATCH',
            body: body,
            headers: this.headers(authorization)
        }
    }

    getBody(text, dueDate, open) {
        let body = {
            text: text,
            dueDate: dueDate,
            open: open,
        };
        return JSON.stringify(body);
    }
}

export { UpdateTodoItem };