const baseUrl = 'https://wt-test.jetbrains.space';

const endpoints = {
    todoList: '/api/http/todo',
}

export { baseUrl, endpoints };