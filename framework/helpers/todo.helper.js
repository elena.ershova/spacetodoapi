import { api } from '../controllers/index.js';

let helper = {
    getItemsCount: async () => {
        let response = await api.GetTodoList().getList();
        let responseObj = await response.json();
        return responseObj.data.length;
    },
    getFirstItemId: async () => {
        let response = await api.GetTodoList().getList();
        let responseObj = await response.json();
        if (responseObj.totalCount > 0) {
            return responseObj.data[0].id;
        }
        return new Error('No data foun in todo list!')
    },
    getItemObj: async (id) => {
        let response = await api.GetTodoList().getList();
        let responseObj = await response.json();
        let filtered = await responseObj.data.filter(item => item.id === id);
        if (filtered !== undefined || filtered.length !== 0) {
            return filtered[0];
        }
        return new Error(`No items found for ${id}!`);
    },
    statusToBool: async (status) => {
        return status === 'Open';
    },
    getOpenCount: async () => {
        let response = await api.GetTodoList().getList();
        let responseObj = await response.json();
        return responseObj
            .data
            .filter(dataItem => dataItem._status === 'Open')
            .length;
    }
}

export { helper };